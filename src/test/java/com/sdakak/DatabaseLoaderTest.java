package com.sdakak;

import junit.framework.TestCase;
import org.junit.Test;

public class DatabaseLoaderTest extends TestCase {

    @Test
    public void testTableName() {
        DatabaseLoader dl = new DatabaseLoader();
        InputData d = new InputData(new Spec("abc.csv"));
        assertEquals("abc", dl.getTableName(d));
    }

    @Test
    public void testGetTableCreationString() {
        DatabaseLoader dl = new DatabaseLoader();
        Spec spec = new Spec("abc.csv");
        spec.fields.add(new Field("name", 10, "TEXT"));
        spec.fields.add(new Field("lname", 10, "TEXT"));
        spec.fields.add(new Field("count", 3, "INTEGER"));
        spec.fields.add(new Field("valid", 1, "BOOLEAN"));
        InputData d = new InputData(spec);
        assertEquals("CREATE TABLE abc(name varchar(255), lname varchar(255), count INTEGER, valid BOOLEAN);", dl.getTableCreationString(d));
    }

    @Test
    public void testGetSqlColumnsText() {
        DatabaseLoader dl = new DatabaseLoader();
        Field f1 = new Field("name", 10, "TEXT");
        assertEquals("name varchar(255)", dl.getSqlColumns(f1));
    }

    @Test
    public void testGetSqlColumnsBoolean() {
        DatabaseLoader dl = new DatabaseLoader();
        Field f2 = new Field("valid", 1, "BOOLEAN");
        assertEquals("valid BOOLEAN", dl.getSqlColumns(f2));
    }

    @Test
    public void testGetSqlValuesToInsertFourColumns() {
        DatabaseLoader dl = new DatabaseLoader();
        Spec spec = new Spec("abc.csv");
        spec.fields.add(new Field("name", 10, "TEXT"));
        spec.fields.add(new Field("lname", 10, "TEXT"));
        spec.fields.add(new Field("count", 3, "INTEGER"));
        spec.fields.add(new Field("valid", 1, "BOOLEAN"));
        InputData d = new InputData(spec);
        String line = "James     Pollock   10 1";
        assertEquals("INSERT INTO abc VALUES ('James', 'Pollock', 10, TRUE);", dl.getSqlValuesToInsert(d, line));
    }

    @Test
    public void testGetSqlValuesToInsertThreeColumns() {
        DatabaseLoader dl = new DatabaseLoader();
        Spec spec = new Spec("abc.csv");
        spec.fields.add(new Field("name", 10, "TEXT"));
        spec.fields.add(new Field("valid", 1, "BOOLEAN"));
        spec.fields.add(new Field("count", 3, "INTEGER"));
        InputData d = new InputData(spec);
        String line = "John      09  ";
        assertEquals("INSERT INTO abc VALUES ('John', FALSE, 9);", dl.getSqlValuesToInsert(d, line));
    }

}
