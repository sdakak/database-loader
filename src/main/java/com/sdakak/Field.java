package com.sdakak;

/*
Simple object encapsulating a SQL column definition (name, width and type)
 */
class Field {
    String columnName;
    int columnWidth;
    String type;

    public Field(String columnName, int columnWidth, String type) {
        this.columnName = columnName;
        this.columnWidth = columnWidth;
        this.type = type.toUpperCase();
    }

    @Override
    public String toString() {
        return "Field{" +
                "columnName='" + columnName + '\'' +
                ", columnWidth=" + columnWidth +
                ", type='" + type + '\'' +
                '}';
    }
}
