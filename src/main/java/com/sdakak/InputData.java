package com.sdakak;

import java.util.ArrayList;
import java.util.List;

/*
This is the main object that has the spec definition and all the input data files that belong to it
 */
class InputData {
    Spec spec;
    List<String> inputFiles;

    public InputData(Spec spec) {
        this.spec = spec;
        this.inputFiles = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Data{" +
                "spec='" + spec + '\'' +
                ", data=" + fileNamesPrint() +
                '}';
    }

    public String fileNamesPrint() {
        String res = "";
        for (String s : inputFiles) {
            res += s + ", ";
        }
        return res;
    }
}
