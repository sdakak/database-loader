package com.sdakak;

import java.util.ArrayList;
import java.util.List;

/*
Spec object encapsulates the spec defenition filename and all the fields defined in it
 */
class Spec {
    String filename;
    List<Field> fields;

    public Spec(String filename) {
        this.filename = filename;
        this.fields = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Spec{" +
                "filename='" + filename + '\'' +
                ", fields=" + fields +
                '}';
    }
}
