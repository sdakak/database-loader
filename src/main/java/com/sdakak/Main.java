package com.sdakak;

public class Main {

  public static void main(String args[]) throws Exception {
      DatabaseLoader loader = new DatabaseLoader();
      loader.run();

      // Result of above command is that the required sql tables are created, records inserted and System.out shows the
      // SQL commands run. Specifically the System.out is
      /*
        Executing: CREATE TABLE testformat2(name varchar(255), lname varchar(255), count INTEGER, valid BOOLEAN);
        Executing: INSERT INTO testformat2 VALUES ('James', 'Pollock', 10, TRUE);
        Executing: INSERT INTO testformat2 VALUES ('John', 'Boggs', 9, FALSE);
        Executing: INSERT INTO testformat2 VALUES ('Jack', 'White', -18, FALSE);

        Executing: CREATE TABLE testformat1(name varchar(255), valid BOOLEAN, count INTEGER);
        Executing: INSERT INTO testformat1 VALUES ('James', TRUE, 10);
        Executing: INSERT INTO testformat1 VALUES ('John', FALSE, 9);
        Executing: INSERT INTO testformat1 VALUES ('Jack', TRUE, -8);
        Executing: INSERT INTO testformat1 VALUES ('Foonyor', TRUE, 1);
        Executing: INSERT INTO testformat1 VALUES ('Barzane', FALSE, -12);
        Executing: INSERT INTO testformat1 VALUES ('Quuxitude', TRUE, 103);
       */
  }
}
