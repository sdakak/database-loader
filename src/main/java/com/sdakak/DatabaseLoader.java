package com.sdakak;

import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class DatabaseLoader {
    final String specsDir = "src/main/resources/specs";
    final String dataDir = "src/main/resources/data";
    final String username = "root";
    final String password = "8Gc$yEF9Dkq9"; //TODO in prod read these from envt variables or secure key stores

    /*
    Entry point for the class. Reads the specs and data files and inserts them in DB
     */
    public void run() throws FileNotFoundException {
        Map<String, Spec> specs = getAllSpecs();
        List<InputData> data = getAllInputDataFiles(specs);
        Sql2o sql2o = new Sql2o("jdbc:mysql://localhost:3306/test1?useLegacyDatetimeCode=false&serverTimezone=UTC",
                username, password);
        loadDataToDb(data, sql2o);
    }

    /*
    Given an InputData object that describes each spec and the input files that go with it, create the tables and
    insert the values
     */
    private void loadDataToDb(List<InputData> data, Sql2o sql) throws FileNotFoundException {
        try (Connection con = sql.open()) {
            for (InputData d : data) {
                // CREATE TABLE
                String createTableSql = getTableCreationString(d);
                System.out.println("Executing: " + createTableSql);
                con.createQuery(createTableSql).executeUpdate();

                // INSERT VALUES
                for (String fileName : d.inputFiles) {
                    Scanner scan = new Scanner(new File(dataDir + "/" + fileName));
                    while(scan.hasNext()) {
                        String line = scan.nextLine();
                        String insertSql = getSqlValuesToInsert(d, line);
                        System.out.println("Executing: " + insertSql);
                        con.createQuery(insertSql).executeUpdate();
                    }
                }
                System.out.println();
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            throw new IllegalArgumentException();
        }

    }

    /*
    InputData stores spec.filename as abc.csv but our tablename is just abc. So this returns abc
     */
    protected String getTableName(InputData d) {
        return d.spec.filename.split("\\.")[0];
    }

    /*
    Given InputData.spec which describes the format of data, return a CREATE TABLE Sql Statement that corresponds to it
    So, Given spec:
     filename = testformat2.csv
     fields [
        name 10 text
        lname 10 text
        count 3 integer
        valid 1 boolean
     ]
    return:
     CREATE TABLE testformat2(name varchar(255), lname varchar(255), count INTEGER, valid BOOLEAN);
     */
    protected String getTableCreationString(InputData d) {
        String tName = getTableName(d);
        String createTableSql = "CREATE TABLE " + tName + "(";

        for (int i = 0; i < d.spec.fields.size(); i++) {
            createTableSql += getSqlColumns(d.spec.fields.get(i));
            if (i != d.spec.fields.size() - 1) {
                createTableSql += ", ";
            }
        }
        createTableSql += ");";
        return createTableSql;
    }

    /*
    Given an object describing the fields get the sql column creation string.
    So, given columnName = name columnType = TEXT return
        name varchar(255)
     */
    protected String getSqlColumns(Field field) {
        String res = "";
        res += field.columnName + " ";
        if (field.type.equals("TEXT")) {
            res += "varchar(255)";
        } else if (field.type.equals("INTEGER")) {
            res += "INTEGER";
        } else if (field.type.equals("BOOLEAN")) {
            res += "BOOLEAN";
        } else {
            throw new IllegalArgumentException("##### Unsupported column type found: " + field.type);
        }
        return res;
    }

    /*
    Given a line inside data file and spec about how to read it convert it SQL INSERT statement
    So
    Spec:
       name 10 text
       lname 10 text
       count 3 integer
       valid 1 boolean
    And Line:
       James     Pollock   101
    Becomes:
       INSERT INTO testformat2 VALUES ('James', 'Pollock', 10, TRUE);
     */
    protected String getSqlValuesToInsert(InputData d, String line) {
        String insertSql = "INSERT INTO " + getTableName(d) + " VALUES (";
        String value = "";
        for (int i = 0, j = 0; i < d.spec.fields.size(); i++) {
            Field field = d.spec.fields.get(i);
            if (field.type.equals("TEXT")) {
                value += "'";
            }
            if (field.type.equals("BOOLEAN")) {
                if (line.charAt(j) == '1') {
                    value += "TRUE";
                } else if (line.charAt(j) == '0') {
                    value += "FALSE";
                } else {
                    throw new IllegalArgumentException("Unrecognized boolean value in was " + line.charAt(j));
                }
            } else {
                value += line.substring(j, Math.min(j+field.columnWidth, line.length())).trim();
            }
            if (field.type.equals("TEXT")) {
                value += "'";
            }
            if (i != d.spec.fields.size() - 1) {
                value += ", ";
            }
            j += field.columnWidth;
        }
        insertSql += value;
        insertSql += ");";
        return insertSql;
    }

    /*
    Read all the specs from resources/specs directory
     */
    private Map<String, Spec> getAllSpecs() throws FileNotFoundException {
        Map<String, Spec> specs = new HashMap<>();
        List<String> files = Arrays.asList(new File(specsDir).list());
        Scanner scan;
        for (String s : files) {
            Spec spec = new Spec(s);
            scan = new Scanner(new File(specsDir + "/" + s));
            scan.nextLine();
            populateSpec(spec, scan);
            specs.put(spec.filename, spec);
        }
        return specs;
    }

    /*
    Populate the spec object with all column definitions from a file
    So line that reads name,10,TEXT becomes
    spec.columnName = name, spec.columnWidth = 10, spec.columnType = TEXT
     */
    private void populateSpec(Spec spec, Scanner scan) {
        while (scan.hasNext()) {
            String tokens[] = scan.next().split(",");
            Field field = new Field(tokens[0], Integer.parseInt(tokens[1]), tokens[2]);
            spec.fields.add(field);
        }
    }

    /*
    Given a spec object and known location of input data files, create a InputData object which contains a spec
    and all the input files that go with it. This will be used by the loadToDB method
    This converts the input files and spec files that are all mixed to an object that looks like:
    First spec definition, list of input files that go with it
    Another spec definition, list of input files that go with that one,
    ...., etc
     */
    private List<InputData> getAllInputDataFiles(Map<String, Spec> specs) {
        List<InputData> data = new ArrayList<>();
        List<String> files;

        files = Arrays.asList(new File(dataDir).list());
        InputData currentInputData = null;
        for (String s : files) {
            String[] tokens = s.split("_");
            if (currentInputData != null && (tokens[0] + ".csv").equals(currentInputData.spec.filename)) {
                currentInputData.inputFiles.add(s);
            } else {
                if (currentInputData != null) {
                    data.add(currentInputData);
                }
                currentInputData = new InputData(specs.get(tokens[0] + ".csv"));
                currentInputData.inputFiles.add(s);
            }
        }
        data.add(currentInputData);

        return data;
    }
}

