Database Loader
===============

Introduction
------------
A simple java project to create tables and insert arbitrary records into sql db based on definitions provided.

DB Definitions
--------------
- The specs are in src/main/resources/specs. Here is a sample spec file. It's called testformat1.csv
```
"column name",width,datatype
name,10,TEXT
valid,1,BOOLEAN
count,3,INTEGER
```
- The data is in src/main/resources/data. This data file is called testformat1-2018-05-01.txt. All the data files with the same prefix as the above spec file will be loaded into the same table
```
Foonyor 1 1
Barzane 0-12
Quuxitude 1103
```

The output table looks like:
```
+-----------+-------+-------+
| name      | valid | count |
+-----------+-------+-------+
| Foonyor   |     1 |     1 |
| Barzane   |     0 |   -12 |
| Quuxitude |     1 |   103 |
+-----------+-------+-------+

```
To run
------

- Clone repo
- Change config: DatabaseLoader has constants that stores your db name, username and password. These will have to be modified to match your environment
- mvn clean install 
- java -jar target/<filename>.jar

Here is the output when the program is run:

![program output](src/main/resources/screenshots/output.png "program output")

And the changes to MySQL DB are shown here:

![sql statemnets run](src/main/resources/screenshots/sql.png "sql statemnets run")

It also comes with unit tests:

![unit tests](src/main/resources/screenshots/tests.png "unit tests")

The code is extensively documented. A good place to start are the unit tests in src/test/java and then the DatabaseLoader class. The code is extensively commented

How does it work
----------------
When you call loader.run():

- The program first tries to create a Spec object which describes the structure of your data
- It then associates the Spec object with specific input data files that belong to it
- It then does the same for all of the tables you want to create
- Finally it generates SQL CREATE TABLE and INSERT INTO TABLE statements based on the above two objects and runs it